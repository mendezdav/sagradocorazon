-- MySQL dump 10.14  Distrib 5.5.37-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: sagradocorazon
-- ------------------------------------------------------
-- Server version	5.5.37-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alumno_grado`
--

DROP TABLE IF EXISTS `alumno_grado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alumno_grado` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `id_alumno` int(8) NOT NULL,
  `id_grado` int(8) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_alumno` (`id_alumno`),
  KEY `id_grado` (`id_grado`),
  CONSTRAINT `alumno_grado_ibfk_2` FOREIGN KEY (`id_grado`) REFERENCES `grado` (`id`),
  CONSTRAINT `alumno_grado_ibfk_1` FOREIGN KEY (`id_alumno`) REFERENCES `alumnos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1 COMMENT='tabla alumno_grado';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alumno_grado`
--

LOCK TABLES `alumno_grado` WRITE;
/*!40000 ALTER TABLE `alumno_grado` DISABLE KEYS */;
/*!40000 ALTER TABLE `alumno_grado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alumnos`
--

DROP TABLE IF EXISTS `alumnos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alumnos` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `primer_nombre` varchar(15) NOT NULL,
  `segundo_nombre` varchar(15) NOT NULL,
  `primer_apellido` varchar(15) NOT NULL,
  `segundo_apellido` varchar(15) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `sexo` varchar(1) NOT NULL,
  `telefono` int(8) NOT NULL,
  `telefono_opcional` int(8) DEFAULT NULL,
  `direccion` varchar(65) NOT NULL,
  `responsable` varchar(60) NOT NULL,
  `telefono_responsable` int(8) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 COMMENT='tabla alumnos';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alumnos`
--

LOCK TABLES `alumnos` WRITE;
/*!40000 ALTER TABLE `alumnos` DISABLE KEYS */;
INSERT INTO `alumnos` VALUES (1,'reynaldo','alfonso','diaz','berrios','1994-03-11','M',2297766,22945142,'urb las magnolias calle los jazmines casa 86 soyapango','Bessie Cecilia Berrios de Diaz',78502543),(2,'karla','yesenia','pinto','sanchez','1987-06-19','F',22931223,0,'urb las margaritas pol 9 calle florida','Juan Carlos Pinto',22567686),(11,'mari','Del Carmen','Berrios','Diaz','1996-11-05','F',22789890,78245678,'urb bueno lindo, calle mercedez','reynaldo diaz',78502543);
/*!40000 ALTER TABLE `alumnos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asignatura`
--

DROP TABLE IF EXISTS `asignatura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asignatura` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 COMMENT='tabla asignatura';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asignatura`
--

LOCK TABLES `asignatura` WRITE;
/*!40000 ALTER TABLE `asignatura` DISABLE KEYS */;
INSERT INTO `asignatura` VALUES (3,'sociales'),(4,'ciencias'),(5,'matematicas'),(6,'lenguaje');
/*!40000 ALTER TABLE `asignatura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asignatura_grado`
--

DROP TABLE IF EXISTS `asignatura_grado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asignatura_grado` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `id_asignatura` int(8) NOT NULL,
  `id_grado` int(8) NOT NULL,
  `id_docente` int(8) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_asignatura` (`id_asignatura`),
  KEY `id_grado` (`id_grado`),
  KEY `id_docente` (`id_docente`),
  CONSTRAINT `asignatura_grado_ibfk_3` FOREIGN KEY (`id_docente`) REFERENCES `docente` (`id`),
  CONSTRAINT `asignatura_grado_ibfk_1` FOREIGN KEY (`id_asignatura`) REFERENCES `asignatura` (`id`),
  CONSTRAINT `asignatura_grado_ibfk_2` FOREIGN KEY (`id_grado`) REFERENCES `grado` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asignatura_grado`
--

LOCK TABLES `asignatura_grado` WRITE;
/*!40000 ALTER TABLE `asignatura_grado` DISABLE KEYS */;
/*!40000 ALTER TABLE `asignatura_grado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `docente`
--

DROP TABLE IF EXISTS `docente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docente` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `primer_nombre` varchar(15) NOT NULL,
  `segundo_nombre` varchar(15) NOT NULL,
  `primer_apellido` varchar(15) NOT NULL,
  `segundo_apellido` varchar(15) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `sexo` varchar(1) NOT NULL,
  `telefono` varchar(9) DEFAULT NULL,
  `telefono_opcional` varchar(9) DEFAULT NULL,
  `direccion` varchar(65) NOT NULL,
  `dui` varchar(10) DEFAULT NULL,
  `nit` varchar(17) DEFAULT NULL,
  `usuario` varchar(20) DEFAULT NULL,
  `adminrights` tinyint(1) DEFAULT '0',
  `clave` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `docente`
--

LOCK TABLES `docente` WRITE;
/*!40000 ALTER TABLE `docente` DISABLE KEYS */;
INSERT INTO `docente` VALUES (1,'walter','ovidio','sanchez','cardona','2014-04-06','M','22675432',NULL,'urb las margaritas pol 17 #48 soyapango','11011080','2147483647','admin',1,'e10adc3949ba59abbe56e057f20f883e'),(2,'ruth','aida','gonzales ','guerrero','2013-08-20','F','28091234',NULL,'urb prados de venecia pasaje 9 #19 Ilopango','911011580','2147483647','ruth.01',0,'e10adc3949ba59abbe56e057f20f883e'),(3,'Ana ','Raquel','Arevalo','Urquiza','2013-02-19','F','29650998',NULL,'col santa lucia pasaje  M #23 soyapango','411711180','2147483647','ana.01',0,'e10adc3949ba59abbe56e057f20f883e'),(4,'William','David','Parras','Mendez','1993-11-06','M','2277-9948','7044-6578','Col, San Carlos 2','44657918-7','5123-548765-42','william.01',0,'ab1e02fbcd436872f4fee6d472363b3a'),(5,'Karla','Yesenia','Pinto','Sanchez','1993-02-25','F','2246-5789','7765-4216','Soyapango, San Salvador','42143215-5','5487-621354-59','karla.01',0,'86dc9d9403d82af0580978239f8f0d20');
/*!40000 ALTER TABLE `docente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grado`
--

DROP TABLE IF EXISTS `grado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grado` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(15) NOT NULL,
  `orientador` int(11) DEFAULT NULL,
  `ciclo` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orientador` (`orientador`),
  CONSTRAINT `grado_ibfk_1` FOREIGN KEY (`orientador`) REFERENCES `docente` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1 COMMENT='tabla grado';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grado`
--

LOCK TABLES `grado` WRITE;
/*!40000 ALTER TABLE `grado` DISABLE KEYS */;
INSERT INTO `grado` VALUES (15,'Primer Grado',1,1),(16,'Segundo Grado',2,1),(17,'Tercer Grado',3,1);
/*!40000 ALTER TABLE `grado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `horario`
--

DROP TABLE IF EXISTS `horario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `horario` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `id_asignatura_grado` int(8) NOT NULL,
  `dia` varchar(9) NOT NULL,
  `inicio` time NOT NULL,
  `fin` time NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_asignatura_grado` (`id_asignatura_grado`),
  CONSTRAINT `horario_ibfk_1` FOREIGN KEY (`id_asignatura_grado`) REFERENCES `asignatura_grado` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `horario`
--

LOCK TABLES `horario` WRITE;
/*!40000 ALTER TABLE `horario` DISABLE KEYS */;
/*!40000 ALTER TABLE `horario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `system`
--

DROP TABLE IF EXISTS `system`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `system`
--

LOCK TABLES `system` WRITE;
/*!40000 ALTER TABLE `system` DISABLE KEYS */;
/*!40000 ALTER TABLE `system` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-05-02 21:44:25
