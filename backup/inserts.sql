
--
-- Dumping data for table `alumno_grado`
--

LOCK TABLES `alumno_grado` WRITE;
/*!40000 ALTER TABLE `alumno_grado` DISABLE KEYS */;
INSERT INTO `alumno_grado` VALUES (19,11,5),(22,1,5),(24,2,7);
/*!40000 ALTER TABLE `alumno_grado` ENABLE KEYS */;
UNLOCK TABLES;



--
-- Dumping data for table `alumnos`
--

LOCK TABLES `alumnos` WRITE;
/*!40000 ALTER TABLE `alumnos` DISABLE KEYS */;
INSERT INTO `alumnos` VALUES (1,'reynaldo','alfonso','diaz','berrios','1994-03-11','M',2297766,22945142,'urb las magnolias calle los jazmines casa 86 soyapango','Bessie Cecilia Berrios de Diaz',78502543),(2,'karla','yesenia','pinto','sanchez','1987-06-19','F',22931223,0,'urb las margaritas pol 9 calle florida','Juan Carlos Pinto',22567686),(11,'mari','Del Carmen','Berrios','Diaz','1996-11-05','F',22789890,78245678,'urb bueno lindo, calle mercedez','reynaldo diaz',78502543);
/*!40000 ALTER TABLE `alumnos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `asignatura`
--

LOCK TABLES `asignatura` WRITE;
/*!40000 ALTER TABLE `asignatura` DISABLE KEYS */;
INSERT INTO `asignatura` VALUES (3,'sociales'),(4,'ciencias'),(5,'matematicas'),(6,'lenguaje');
/*!40000 ALTER TABLE `asignatura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `asignatura_grado`
--

LOCK TABLES `asignatura_grado` WRITE;
/*!40000 ALTER TABLE `asignatura_grado` DISABLE KEYS */;
INSERT INTO `asignatura_grado` VALUES (1,3,4,1),(2,6,4,2),(12,5,5,3);
/*!40000 ALTER TABLE `asignatura_grado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `docente`
--

LOCK TABLES `docente` WRITE;
/*!40000 ALTER TABLE `docente` DISABLE KEYS */;
INSERT INTO `docente` VALUES (1,'walter','ovidio','sanchez','cardona','2014-04-06','M','22675432',NULL,'urb las margaritas pol 17 #48 soyapango','11011080','2147483647','admin',1,'e10adc3949ba59abbe56e057f20f883e'),(2,'ruth','aida','gonzales ','guerrero','2013-08-20','F','28091234',NULL,'urb prados de venecia pasaje 9 #19 Ilopango','911011580','2147483647','ruth.01',0,'e10adc3949ba59abbe56e057f20f883e'),(3,'Ana ','Raquel','Arevalo','Urquiza','2013-02-19','F','29650998',NULL,'col santa lucia pasaje  M #23 soyapango','411711180','2147483647','ana.01',0,'e10adc3949ba59abbe56e057f20f883e'),(4,'William','David','Parras','Mendez','1993-11-06','M','2277-9948','7044-6578','Col, San Carlos 2','44657918-7','5123-548765-42','william.01',0,'ab1e02fbcd436872f4fee6d472363b3a'),(5,'Karla','Yesenia','Pinto','Sanchez','1993-02-25','F','2246-5789','7765-4216','Soyapango, San Salvador','42143215-5','5487-621354-58','karla.01',0,'86dc9d9403d82af0580978239f8f0d20'),(6,'Reynaldo','alfonso','Diaz','Berrios','2014-05-02','M','2252-4654','7789-5454','San Salvador','54321546-5','5452-218787-68','reynaldo.01',0,'b125c70b5931674bff69a4e2eafdb83d');
/*!40000 ALTER TABLE `docente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `grado`
--

LOCK TABLES `grado` WRITE;
/*!40000 ALTER TABLE `grado` DISABLE KEYS */;
INSERT INTO `grado` VALUES (15,'Primer Grado','1',1),(16,'Segundo Grado','2',1),(17,'Tercer Grado','3',1);
/*!40000 ALTER TABLE `grado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `horario`
--

LOCK TABLES `horario` WRITE;
/*!40000 ALTER TABLE `horario` DISABLE KEYS */;
/*!40000 ALTER TABLE `horario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `system`
--

LOCK TABLES `system` WRITE;
/*!40000 ALTER TABLE `system` DISABLE KEYS */;
/*!40000 ALTER TABLE `system` ENABLE KEYS */;
UNLOCK TABLES;