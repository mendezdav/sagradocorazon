<?php
    class ficha_alumno_hModel extends object{
        
        public function getIdByAlumno($id_alumno){
            $anio = date("Y");
            $query = "SELECT id FROM ficha_alumno_h WHERE id_alumno=$id_alumno AND anio='{$anio}'";
            data_model()->executeQuery($query);
            if(data_model()->getNumRows()<=0){
                return -1;
            }else{
                $rs = data_model()->getResult()->fetch_assoc();
                return $rs['id'];
            }
                
        }
        
        public function obtener_detalle($id_ficha){
            $query = "SELECT * FROM ficha_alumno_d JOIN asignatura ON asignatura.id = id_asignatura WHERE id_ficha_h=$id_ficha";
            return data_model()->cacheQuery($query);
        }
    }
?>
