<?php
	
	function template(){
		
		return BM::singleton()->getObject('temp');
		
	}
	
	function page(){
		
		return BM::singleton()->getObject('temp')->getPage();
		
	}
	
	function data_model(){
		
		return BM::singleton()->getObject('db');
		
	}

  function isAdmin(){
    if(Session::getLevel() == 1) {
        page()->addEstigma('isAdmin', true);
      }else{
        page()->addEstigma('isAdmin', false);
      }
  }
  
  function generar_fichas(){
      $anio = date("Y");
      
      /* Seleccion de alumnos */
      $query = "SELECT id FROM alumnos";
      $bufferAlumnos = array();
      $bufferAlumnoGrado = array();
      $bufferGradoAsignatura = array();
      
      data_model()->executeQuery($query);
      
      while($rs = data_model()->getResult()->fetch_assoc()){
          $bufferAlumnos[] = $rs;
      }
      
      foreach ($bufferAlumnos as $alumno){
          $id = $alumno['id'];
          /* Seleccion de grado */
          $query = "SELECT id_grado FROM alumno_grado WHERE id_alumno=$id";
          data_model()->executeQuery($query);
          while($rs = data_model()->getResult()->fetch_assoc()){
                $rs['id_alumno'] = $id;
                $rs['anio']      = $anio;
                $bufferAlumnoGrado[] = $rs;
          }
      }
      
      /* creacion de cabeceras*/
        foreach ($bufferAlumnoGrado as $row){
            $id_alumno = $row['id_alumno'];
            $id_grado  = $row['id_grado'];
            $anio      = $row['anio'];

            $query = "SELECT * FROM ficha_alumno_h WHERE id_alumno=$id_alumno AND id_grado=$id_grado AND anio='{$anio}'";
            data_model()->executeQuery($query);
            $num = data_model()->getNumRows(); 
            if($num<=0){
                $query = "INSERT INTO ficha_alumno_h(id_alumno, id_grado, anio) VALUES($id_alumno, $id_grado, '{$anio}')";
                
                data_model()->executeQuery($query);
                import("objects.ficha_alumno_h");
                $cls = "ficha_alumno_hModel";
                $mdlFicha = new $cls();
                $id_ficha = $mdlFicha->last_insert_id();
                
                $bufferAsig = array();
                $query = "SELECT id_asignatura FROM asignatura_grado WHERE id_grado=$id_grado";
                data_model()->executeQuery($query);
                while($res = data_model()->getResult()->fetch_assoc()){
                    $res['id_ficha'] = $id_ficha;
                    $bufferAsig[]    = $res;
                }
                
                foreach ($bufferAsig as $row2){
                    $id_ficha      = $row2['id_ficha'];
                    $id_asignatura = $row2['id_asignatura'];
                    $query = "INSERT INTO ficha_alumno_d(id_ficha_h, id_asignatura) VALUES ($id_ficha, $id_asignatura)";
                    
                    data_model()->executeQuery($query);
                }
            }
        }
  }

  function isSecretary(){
    if(Session::getLevel() == 2) {
        page()->addEstigma('isSecretary', true);
      }else{
        page()->addEstigma('isSecretary', false);
      }
  }
	
	function set_type($data){
		
    $data = (is_string($data)) ? data_model()->sanitizeData($data):$data;
    $val  = (is_string($data)) ? "'$data'":$data;
    
    return $val; 
		
	}

	function _updatedata($object,$tblname=''){
		header('Content-type:text/javascript;charset=UTF-8');
		$tblname = addslashes($tblname);
    $json=json_decode(stripslashes($_POST["_gt_json"]));
    if($json->{'action'} == 'save'):
      $sql = "";
      $params = array();
      $errors = "";
  
      //deal with those deleted
      $deletedRecords = $json->{'deletedRecords'};
      foreach ($deletedRecords as $value):
        $id = $object->model->get_child($tblname)->getId();
        $object->model->get_child($tblname)->delete($value->$id,$id);
      endforeach;
  
      //deal with those updated
      $updatedRecords = $json->{'updatedRecords'};
      foreach ($updatedRecords as $value):
        $id = $object->model->get_child($tblname)->getId();
        $data = get_object_vars($value);
        $model = $object->model->get_child($tblname);
        $model->get($data[$id]);
        $model->change_status($data);
        $model->save();
      endforeach;
  
      //deal with those inserted
      $insertedRecords = $json->{'insertedRecords'};
      foreach ($insertedRecords as $value):
        $data = get_object_vars($value);
        $model = $object->model->get_child($tblname);
        $model->get($data['id']);
        $model->change_status($data);
        $model->save(); 
      endforeach;
  
      $ret = "{success : true,exception:''}";
    endif;
	}

  function _updatedata_cd($object,$tblname,$field,$val){
    header('Content-type:text/javascript;charset=UTF-8');
    $tblname = addslashes($tblname);
    $json=json_decode(stripslashes($_POST["_gt_json"]));
    if($json->{'action'} == 'save'):
      $sql = "";
      $params = array();
      $errors = "";
  
      //deal with those deleted
      $deletedRecords = $json->{'deletedRecords'};
      foreach ($deletedRecords as $value):
        $id = $object->model->get_child($tblname)->getId(); 
        $object->model->get_child($tblname)->delete($value->$id,$id);
        $h_data = array();
        $h_data['usuario'] = Session::getUser();
        $h_data['descripcion'] = "se elimina producto ".$value->$id;
        $h_data['fecha_hora'] = date("y-m-d h:m:s");
        $h_data['modulo'] = "inventario";
        $historial = $object->model->get_child('historial');
        $historial->get(0);
        $historial->change_status($h_data);
        $historial->save();
      endforeach;
  
      //deal with those updated
      $updatedRecords = $json->{'updatedRecords'};
      foreach ($updatedRecords as $value):
        $data = get_object_vars($value);
        $id = $object->model->get_child($tblname)->getId();
        $model = $object->model->get_child($tblname);
        $model->get($data[$id]);
        $model->change_status($data);
        $model->save();
        $h_data = array();
        $h_data['usuario'] = Session::getUser();
        $h_data['descripcion'] = "se actualiza producto ".$data['estilo'];
        $h_data['fecha_hora'] = date("y-m-d h:m:s");
        $h_data['modulo'] = "inventario";
        $historial = $object->model->get_child('historial');
        $historial->get(0);
        $historial->change_status($h_data);
        $historial->save();
      endforeach;
  
      //deal with those inserted
      $insertedRecords = $json->{'insertedRecords'};
      foreach ($insertedRecords as $value):
        $data = get_object_vars($value);
        $model = $object->model->get_child($tblname);
        $model->get($data['id']);
        $data[$field] = $val;
        $model->change_status($data);
        $model->save();
        $h_data = array();
        $h_data['usuario'] = Session::getUser();
        $h_data['descripcion'] = "se inserta producto ".$data['estilo'];
        $h_data['fecha_hora'] = date("y-m-d h:m:s");
        $h_data['modulo'] = "inventario";
        $historial = $object->model->get_child('historial');
        $historial->get(0);
        $historial->change_status($h_data);
        $historial->save(); 
      endforeach;
  
      $ret = "{success : true,exception:''}";
      echo $ret;
    endif;
  }

	function _loaddata($tblname=''){
		header('Content-type:text/javascript;charset=UTF-8');
		$tblname = addslashes($tblname);
    $json=json_decode(stripslashes($_POST["_gt_json"]));
    $pageNo = $json->{'pageInfo'}->{'pageNum'};
    $pageSize = 10;//10 rows per page

    //to get how many records totally.
    $sql = "select count(*) as cnt from $tblname";
    $handle = mysql_query($sql);
    $row = mysql_fetch_object($handle);
    $totalRec = $row->cnt;

    //make sure pageNo is inbound
    if($pageNo<1||$pageNo>ceil(($totalRec/$pageSize))):
      $pageNo = 1;
    endif;

    if($json->{'action'} == 'load'):
      $sql = "select * from $tblname limit " . ($pageNo - 1)*$pageSize . ", " . $pageSize;
      $handle = mysql_query($sql);
      $retArray = array();
      while ($row = mysql_fetch_object($handle)):
        $retArray[] = $row;
      endwhile;
      $data = json_encode($retArray);
      $ret = "{data:" . $data .",\n";
      $ret .= "pageInfo:{totalRowNum:" . $totalRec . "},\n";
      $ret .= "recordType : 'object'}";
      echo $ret;
    endif;
	}

  function _loaddata_cf($tblname='',$field,$value){
    header('Content-type:text/javascript;charset=UTF-8');
    $tblname = addslashes($tblname);
    $json=json_decode(stripslashes($_POST["_gt_json"]));
    $pageNo = $json->{'pageInfo'}->{'pageNum'};
    $pageSize = 10;//10 rows per page

    //to get how many records totally.
    $sql = "select count(*) as cnt from $tblname WHERE {$field} = '{$value}' AND visible=0";
    $handle = mysql_query($sql);
    $row = mysql_fetch_object($handle);
    $totalRec = $row->cnt;

    //make sure pageNo is inbound
    if($pageNo<1||$pageNo>ceil(($totalRec/$pageSize))):
      $pageNo = 1;
    endif;

    if($json->{'action'} == 'load'):
      $sql = "select * from $tblname WHERE {$field} = '{$value}' AND visible=0 limit " . ($pageNo - 1)*$pageSize . ", " . $pageSize;
      $handle = mysql_query($sql);
      $retArray = array();
      while ($row = mysql_fetch_object($handle)):
        $retArray[] = $row;
      endwhile;
      $data = json_encode($retArray);
      $ret = "{data:" . $data .",\n";
      $ret .= "pageInfo:{totalRowNum:" . $totalRec . "},\n";
      $ret .= "recordType : 'object'}";
      echo $ret;
    endif;
  }

  function pagina_simple($rutaHtml,$titulo='',$menu=''){
    template()->buildFromTemplates('template.html');
    page()->setTitle($titulo);
    page()->addEstigma("menu",$menu);
    template()->addTemplateBit('content',$rutaHtml);
    template()->addTemplateBit('footer','footer.html');
    template()->parseOutput();
    template()->parseExtras();
    print page()->getContent();
  }

  function alert($str){
    echo '<script type="text/javascript">alert("'.$str.'");</script>';
  }

  function upload_image($destination_dir,$name_media_field,$productid){
        $tmp_name = $_FILES[$name_media_field]['tmp_name']; 
        if ( is_dir($destination_dir) && is_uploaded_file($tmp_name))
        {      
            $img_file  = 'thumbnail_'.$productid;                      
            $img_type  = $_FILES[$name_media_field]['type'];            
            if (((strpos($img_type, "gif") || strpos($img_type, "jpeg") || strpos($img_type,"jpg")) || strpos($img_type,"png") ))
            {
                if(move_uploaded_file($tmp_name, $destination_dir.'/'.$img_file))
                {                
                    return true;
                }
            }
        }
        return false;
    }

?>