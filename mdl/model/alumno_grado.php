<?php

/* * Clase que manejara el model
 * para modulo alumno grado
 */

class alumno_gradoModel extends object {
    public function obtener_grado($id_alumno){
        $query = "SELECT id_grado FROM alumno_grado WHERE id_alumno=$id_alumno";
        data_model()->executeQuery($query);
        $res   = data_model()->getResult()->fetch_assoc();
        return $res['id_grado'];
    }
    
    public function actualizar_grado($id_alumno, $id_nuevo_grado){
        $query = "SELECT * FROM alumno_grado WHERE id_alumno=$id_alumno";
        data_model()->executeQuery($query);
        if(data_model()->getNumRows()>0){
            $query = "UPDATE alumno_grado set id_grado=$id_nuevo_grado WHERE id_alumno=$id_alumno";
        }else{
            $query = "INSERT INTO alumno_grado(id_grado, id_alumno) VALUES($id_nuevo_grado, $id_alumno)";
        }
        data_model()->executeQuery($query);
    }
}

?>
