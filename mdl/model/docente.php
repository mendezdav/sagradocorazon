<?php

/**
 * 
 */
class docenteModel extends object {

    public function auth_user($user, $password) {
        $usuario = set_type($user);
        $clave = set_type($password);
        $query = "SELECT id, adminrights FROM docente WHERE usuario=$usuario AND clave=$clave";

        data_model()->executeQuery($query);

        if (data_model()->getNumRows() > 0) {

            return array(true, data_model()->getResult()->fetch_assoc());
        }

        return array(false, null);
    }

    public function is_droppable($id) {
        $query = "SELECT id FROM grado WHERE orientador=$id";
        $tg = false;
        $ta = false;

        data_model()->executeQuery($query);

        $tg = (data_model()->getNumRows() > 0);
        $query = "SELECT id FROM asignatura_grado WHERE id_docente=$id";

        data_model()->executeQuery($query);

        $ta = (data_model()->getNumRows() > 0);

        return !($ta || $tg);
    }

    public function getIdByUserName($username) {

        $query = "SELECT id FROM docente WHERE usuario='{$username}'";

        data_model()->executeQuery($query);

        $buffer = array();
        $buffer = data_model()->getResult()->fetch_assoc(); 
        
        return $buffer['id'];
    }

    public function createUsername($id, $name) {
        $generic = strtolower($name);

        if ($id = 0) {

            $query = "SELECT id FROM docente WHERE LOWER(primer_nombre) = '{$generic}'";
            data_model()->executeQuery($query);
            $n = data_model()->getNumRows();
            $n++;

            if ($n < 10) {

                return $generic . ".0" . $n;
            } else {

                return $generic . "." . $n;
            }
        } else {

            $query = "SELECT usuario FROM docente WHERE LOWER(primer_nombre) = '{$generic}' AND id=$id";
            data_model()->executeQuery($query);

            if (data_model()->getNumRows() > 0)
                return data_model()->getResult()->fetch_assoc()['usuario'];
            else {

                $n = data_model()->getNumRows();
                $n++;
                if ($n < 10) {

                    return $generic . ".0" . $n;
                } else {

                    return $generic . "." . $n;
                }
            }
        }
    }

}

?>