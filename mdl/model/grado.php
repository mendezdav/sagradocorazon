<?php

class gradoModel extends object {

    public function data_reporte() {
        $query = "select grado.id as id_grado, nombre, CONCAT(primer_nombre,' ',segundo_nombre, ' ',primer_apellido,' ', segundo_apellido) as nombre_orientador, CASE ciclo WHEN 1 THEN 'Primer Ciclo' WHEN 2 THEN 'Segundo Ciclo' WHEN 3 THEN 'Tercer Cilo' WHEN 4 THEN 'Bachillerato'END as ciclo from grado join docente on orientador = docente.id";
        return data_model()->cacheQuery($query);
    }

}

?>