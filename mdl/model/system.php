<?php

/* definicion de modelo */

class systemModel extends object {

    public function weak_protection() {
        $password = "";
        $usuario  = Session::getUser();
        $query    = "SELECT clave FROM docente WHERE usuario='{$usuario}'";

        data_model()->executeQuery($query);
        
        $buffer   = array();
        $buffer   = data_model()->getResult()->fetch_assoc();
        $password = $buffer['clave'];
        
        if (md5($usuario) == $password)
            return true;

        return false;
    }
    
    public function obtener_cursos($id_docente){
        $query = "SELECT * FROM grado WHERE orientador=$id_docente";
        data_model()->executeQuery($query);
        $numRows = data_model()->getNumRows();
        if($numRows > 0){
            return array(false,  data_model()->cacheQuery($query));
        }else{
            return array(true,-1);
        }
    }
    
     public function obtener_asignaturas($id_docente){
        $query = "SELECT asignatura.id as id, nombre FROM asignatura_grado JOIN asignatura on id_asignatura = asignatura.id WHERE id_docente=$id_docente GROUP BY asignatura.id";
        data_model()->executeQuery($query);
        $numRows = data_model()->getNumRows();
        if($numRows > 0){
            return array(false,  data_model()->cacheQuery($query));
        }else{
            return array(true,-1);
        }
    }
    
    public function gradosAsignatura($id_docente, $id_asignatura){
        $query    = "SELECT grado.id as id,id_asignatura ,grado.nombre as nombre FROM asignatura_grado JOIN grado on id_grado = grado.id where id_docente=$id_docente and id_asignatura=$id_asignatura;";
        
        data_model()->executeQuery($query);
        
        $response = array();
        
        while($res = data_model()->getResult()->fetch_assoc()){
            $response[] = $res;
        }
        
        echo json_encode($response);
    }

}

?>