<?php

/* Definicion de modelo para modulo alumnos */

class alumnosModel extends object {
    
    public function asistencia($id_grado){
   
        $query = "select alumnos.id AS numrow, alumnos.id as id_estudiante, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido  from alumno_grado join alumnos on id_alumno = alumnos.id WHERE id_grado=$id_grado ORDER BY primer_apellido ASC";
        $resp  = array();
        data_model()->executeQuery($query);
        while($rs = data_model()->getResult()->fetch_assoc()){
            $resp[] = $rs;
        }
        
        return array(data_model()->cacheQuery($query), $resp);
    }
}

?>