<?php

/* imports */
import('mdl.model.system');
import('mdl.view.system');

/* definicion de controlador */

class systemController extends controller {

    public function index($params = '') {
        $weakProtection = $this->model->weak_protection();
        $id_docente     = 0;
        $user_name      = Session::getUser();
        $docente_model  = $this->model->get_sibling('docente');
        $id_docente     = $docente_model->getIdByUserName($user_name);
        $cursos         = array();
        $asignaturas    = array();
        
        $cursos['vacio'] = true;
        $cursos['data']  = 0;
        
        $asignaturas['vacio'] = true;
        $asignaturas['data']  = 0;
        
        list($cursos['vacio'], $cursos['data'])           = $this->model->obtener_cursos($id_docente);
        list($asignaturas['vacio'], $asignaturas['data']) = $this->model->obtener_asignaturas($id_docente); 
                
        $this->view->index($weakProtection, $cursos, $asignaturas);
    }
    
    public function gradosAsignatura(){
        if(isset($_POST) && !empty($_POST)){
            
            $id_asignatura  = $_POST['id_asignatura'];
            $docente_model  = $this->model->get_sibling('docente');
            $user_name      = Session::getUser();
            $id_docente     = $docente_model->getIdByUserName($user_name); 
            
            $this->model->gradosAsignatura($id_docente, $id_asignatura);
        }
    }
    
    public function asistencia(){
        if(isset($_GET['id_grado']) && !empty($_GET['id_grado'])){
            $id_grado = addslashes($_GET['id_grado']);
            $gradoMdl = $this->model->get_sibling('grado');
            $alumnMdl = $this->model->get_sibling('alumnos');
            
            if($gradoMdl->exists($id_grado)){
                $gradoMdl->get($id_grado);
                $cache     = array();
                $nmb_grado = $gradoMdl->get_attr('nombre');
                $dataArray = array();
                list($cache[0],$dataArray)  = $alumnMdl->asistencia($id_grado);
                $this->view->imprimir_asistencia($cache, $nmb_grado, $dataArray);
            }
        }
    }
    
    public function inscritos(){
        if(isset($_GET['id_grado']) && !empty($_GET['id_grado'])){
            $id_grado = addslashes($_GET['id_grado']);
            $gradoMdl = $this->model->get_sibling('grado');
            $alumnMdl = $this->model->get_sibling('alumnos');
            
            if($gradoMdl->exists($id_grado)){
                $gradoMdl->get($id_grado);
                $cache     = array();
                $nmb_grado = $gradoMdl->get_attr('nombre');
                $dataArray = array();
                list($cache[0],$dataArray)  = $alumnMdl->asistencia($id_grado);
                $this->view->inscritos($cache, $nmb_grado, $dataArray, $id_grado);
            }
        }
    }
    
    public function ficha(){
        if(isset($_GET['id_alumno']) && !empty($_GET['id_alumno'])){
            $alumnoMdl = $this->model->get_sibling('alumnos');
            $fichaMdl  = $this->model->get_child('ficha_alumno_h');
            $id_alumno = $_GET['id_alumno'];
            $alumnoMdl->get($id_alumno);
            $nombre_alumno = $alumnoMdl->get_attr('primer_apellido')." ".$alumnoMdl->get_attr('segundo_apellido').", ".$alumnoMdl->get_attr('primer_nombre')." ".$alumnoMdl->get_attr('segundo_nombre');
            if($alumnoMdl->exists($id_alumno)){
                $id_ficha = $fichaMdl->getIdByAlumno($id_alumno);
                $fichaMdl->get($id_ficha);
                $id_grado = $fichaMdl->get_attr('id_grado');
                $gradoMdl = $this->model->get_sibling('grado');
                $gradoMdl->get($id_grado);
                $nombre_grado = $gradoMdl->get_attr('nombre');
                $docenteMdl = $this->model->get_sibling('docente');
                $id_docente = $docenteMdl->getIdByUserName(Session::getUser());
                $docenteMdl->get($id_docente);
                $nombre_docente = $docenteMdl->get_attr('primer_apellido')." ".$docenteMdl->get_attr('segundo_apellido').", ".$docenteMdl->get_attr('primer_nombre')." ".$docenteMdl->get_attr('segundo_nombre');
                if($id_ficha > 0){
                    $cache    = array();
                    $cache[0] = $fichaMdl->obtener_detalle($id_ficha);
                    $this->view->ficha($cache, $nombre_alumno, $nombre_grado, $nombre_docente, $id_alumno, $id_grado);
                }else{
                    HttpHandler::redirect('/sagradocorazon/system/index');
                }
            }else{
                HttpHandler::redirect('/sagradocorazon/system/index');
            }
        }
    }
    
    public function informe(){
        if(isset($_GET['id_alumno']) && !empty($_GET['id_alumno'])){
            $alumnoMdl = $this->model->get_sibling('alumnos');
            $fichaMdl  = $this->model->get_child('ficha_alumno_h');
            $id_alumno = $_GET['id_alumno'];
            $alumnoMdl->get($id_alumno);
            $nombre_alumno = $alumnoMdl->get_attr('primer_apellido')." ".$alumnoMdl->get_attr('segundo_apellido').", ".$alumnoMdl->get_attr('primer_nombre')." ".$alumnoMdl->get_attr('segundo_nombre');
            if($alumnoMdl->exists($id_alumno)){
                $id_ficha = $fichaMdl->getIdByAlumno($id_alumno);
                $fichaMdl->get($id_ficha);
                $id_grado = $fichaMdl->get_attr('id_grado');
                $gradoMdl = $this->model->get_sibling('grado');
                $gradoMdl->get($id_grado);
                $nombre_grado = $gradoMdl->get_attr('nombre');
                $docenteMdl = $this->model->get_sibling('docente');
                $id_docente = $docenteMdl->getIdByUserName(Session::getUser());
                $docenteMdl->get($id_docente);
                $nombre_docente = $docenteMdl->get_attr('primer_apellido')." ".$docenteMdl->get_attr('segundo_apellido').", ".$docenteMdl->get_attr('primer_nombre')." ".$docenteMdl->get_attr('segundo_nombre');
                if($id_ficha > 0){
                    $cache    = array();
                    $cache[0] = $fichaMdl->obtener_detalle($id_ficha);
                    $this->view->informe($cache, $nombre_alumno, $nombre_grado, $nombre_docente, $id_alumno);
                }else{
                    HttpHandler::redirect('/sagradocorazon/system/index');
                }
            }else{
                HttpHandler::redirect('/sagradocorazon/system/index');
            }
        }
    }
    
    public function cargar_detalle_nota(){
        $id_ficha_alumno_d = $_POST['id_ficha_alumno_d'];
        $n_periodo         = $_POST['n_periodo'];
        $ficha_d           = $this->model->get_child('ficha_alumno_d');
        $ficha_d->get($id_ficha_alumno_d);
        $response = array();
        $response['cp'] = $ficha_d->get_attr('cp'.$n_periodo);
        $response['lp'] = $ficha_d->get_attr('lp'.$n_periodo);
        $response['tp'] = $ficha_d->get_attr('tp'.$n_periodo);
        $response['ap'] = $ficha_d->get_attr('ap'.$n_periodo);
        $response['ep'] = $ficha_d->get_attr('ep'.$n_periodo);
        
        echo json_encode($response);
    }
    
    public function salvar_detalle_nota(){
        $id_ficha_alumno_d = $_POST['id_ficha_alumno_d'];
        $n_periodo         = $_POST['n_periodo'];
        $ficha_d           = $this->model->get_child('ficha_alumno_d');
        $ficha_d->get($id_ficha_alumno_d);
        $cp = $_POST['cp'];
        $lp = $_POST['lp'];
        $tp = $_POST['tp'];
        $ap = $_POST['ap'];
        $ep = $_POST['ep'];
        
        $ficha_d->set_attr('cp'.$n_periodo, $cp);
        $ficha_d->set_attr('lp'.$n_periodo, $lp);
        $ficha_d->set_attr('tp'.$n_periodo, $tp);
        $ficha_d->set_attr('ap'.$n_periodo, $ap);
        $ficha_d->set_attr('ep'.$n_periodo, $ep);
        
        $pp = ($cp*0.10) + ($lp*0.10) + ($tp*0.10) + ($ap*0.20) + ($ep*0.5);
        $ficha_d->set_attr('pp'.$n_periodo, $pp);
        
        $pp1 = $ficha_d->get_attr('pp1');
        $pp2 = $ficha_d->get_attr('pp2');
        $pp3 = $ficha_d->get_attr('pp3');
        $t1  = ($pp1+$pp2+$pp3)/3;
        $pp4 = $ficha_d->get_attr('pp4');
        $pp5 = $ficha_d->get_attr('pp5');
        $pp6 = $ficha_d->get_attr('pp6');
        $t2  = ($pp4+$pp5+$pp6)/3;
        $pp7 = $ficha_d->get_attr('pp7');
        $pp8 = $ficha_d->get_attr('pp8');
        $pp9 = $ficha_d->get_attr('pp9');
        $t3  = ($pp7+$pp8+$pp9)/3;
                
        $pf = ($t1*0.30)+($t2*0.35)+($t3*0.35);
        
        $ficha_d->set_attr('pf', $pf);
        
        $ficha_d->save();
        echo json_encode(array());
    }
    
    public function calificar(){
        if(isset($_GET['id_alumno']) && !empty($_GET['id_alumno']) && isset($_GET['id_asignatura']) && !empty($_GET['id_asignatura']) && isset($_GET['id_grado']) && !empty($_GET['id_grado'])){
            $id_asignatura = $_GET['id_asignatura'];
            $id_alumno     = $_GET['id_alumno'];
            $id_grado     = $_GET['id_grado'];
            $alumnoMdl = $this->model->get_sibling('alumnos');
            $gradoMdl = $this->model->get_sibling('grado');
            $alumnoMdl->get($id_alumno);
            $gradoMdl->get($id_grado);
            $nombre_grado=$gradoMdl->get_attr('nombre');
            $nombre_alumno = $alumnoMdl->get_attr('primer_apellido')." ".$alumnoMdl->get_attr('segundo_apellido').", ".$alumnoMdl->get_attr('primer_nombre')." ".$alumnoMdl->get_attr('segundo_nombre');
            $ficha_h    = $this->model->get_child('ficha_alumno_h');
            $ficha_d    = $this->model->get_child('ficha_alumno_d');
            $id_ficha_h = $ficha_h->getIdByAlumno($id_alumno);
            $id_ficha_d = $ficha_d->getIdByAsignatura($id_ficha_h, $id_asignatura);
            $cache = array();
            $cache[0] = $ficha_d->getCacheByAsignatura($id_ficha_h, $id_asignatura);
            $this->view->calificar($cache, $id_ficha_d, $nombre_alumno, $nombre_grado);
        }
    }


    public function evaluaciones(){
        if(isset($_GET['id_asignatura']) && isset($_GET['id_grado']) && !empty($_GET['id_asignatura']) && !empty($_GET['id_grado'])){ 
            $user_name  = Session::getUser();
            $docenteMdl = $this->model->get_sibling('docente');
            $asignatMdl = $this->model->get_sibling('asignatura');
            $gradoMdl   = $this->model->get_sibling('grado');
            $alumnMdl   = $this->model->get_sibling('alumnos');

            $id_docente = $docenteMdl->getIdByUserName($user_name);
            $id_asignat = $_GET['id_asignatura'];
            $id_grado   = $_GET['id_grado'];
            
            $asignatMdl->get($id_asignat);
            $gradoMdl->get($id_grado);
            
            $nombre_asignatura = $asignatMdl->get_attr('nombre');
            $nombre_grado      = $gradoMdl->get_attr('nombre');
            
            $quantifyArray = array(
                'id_docente'=>$id_docente,
                'id_asignatura'=>$id_asignat,
                'id_grado'=>$id_grado
            );
            
            $asig_graMdl = $this->model->get_sibling('asignatura_grado');
            $numRegs     = $asig_graMdl->MultyQuantify($quantifyArray, true);
            
            if($numRegs > 0)
            {   
                $cache    = array();
                list($cache[0],$dataArray)  = $alumnMdl->asistencia($id_grado);
                $this->view->evaluaciones($nombre_asignatura, $nombre_grado, $dataArray, $cache, $id_asignat, $id_grado);
            }else{
                HttpHandler::redirect('/sagradocorazon/system/index');  
            }
            
        }
    }

    public function login($params = '') {
        $this->view->login();
    }

    public function forbiden() {
        $this->view->forbiden();
    }

    public function account() {
        $this->view->account();
    }

    /**
     * auth_user proceso de autorizacion de usuarios
     * @return null
     */
    public function auth_user() {
        /**
         * $docenteModel modelado de datos de docentes
         * @var object
         */
        $docenteModel = $this->model->get_sibling('docente');

        $usuario = $_POST['username'];
        $clave = md5($_POST['password']);
        list($flg, $data) = $docenteModel->auth_user($usuario, $clave);
        $nivel = $data['adminrights'];
        if ($flg) {

            /* creando las sesiones */
            Session::singleton()->NewSession($usuario, $nivel);

            /* redireccionado */
            HttpHandler::redirect('/sagradocorazon/system/index');
        } else {
            HttpHandler::redirect('/sagradocorazon/system/login?error=WrongAcces');
        }
    }

    public function logout() {
        Session::singleton()->logOut();
        HttpHandler::redirect('/sagradocorazon/system/login');
    }

}

?>