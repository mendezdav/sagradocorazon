<?php

/* Imports necesarios */
import('mdl.model.alumnos');
import('mdl.view.alumnos');

/* Definicion de controlador para modulo */

class alumnosController extends controller {

    public function __construct() {
        if (!Session::singleton()->ValidateSession())
            HttpHandler::redirect('/sagradocorazon/system/login?error=NoAuth');
        if (Session::singleton()->getLevel() != 1 && Session::singleton()->getLevel() != 2)
            HttpHandler::redirect('/sagradocorazon/system/forbiden');
        $this->model = Helper::get_model($this); # load respective module
        $this->view = Helper::get_view($this);  # load respective view
    }

    /* Presentar modulo nuevo alumno */

    public function nuevo_alumno() {
        $grado    = $this->model->get_sibling('grado');
        $cache    = array();
        $cache[0] = $grado->get_list();
        $this->view->agregar_alumno($cache);
    }

    /* Guardar nuevo Alumno */

    public function guardar_alumno() {
        if (isset($_POST) && !empty($_POST)):
            $this->model->get(0);
            $data  = $_POST;
            $grado = $data['grado'];
            unset($data['grado']);
            $al_gd = $this->model->get_sibling('alumno_grado');
            $al_gd->get(0);
            $dtag  = array(); 
            $this->model->change_status($data);
            $this->model->save();
            $dtag['id_alumno'] = $this->model->last_insert_id();
            $dtag['id_grado']  = $grado;
            $al_gd->change_status($dtag);
            $al_gd->save();
            HttpHandler::redirect('/sagradocorazon/alumnos/nuevo_alumno');
        else:
            echo "llamada realizada fuera de la funcion guardar_alumno";
        endif;
    }
    
    /* Actualizar datos luego de editarlos */

    public function actualizar() {
        if (isset($_POST) && !empty($_POST)):
            $id = isset($_GET['id']) ? $_GET['id'] : 0;
            $this->model->get($id);
            $data            = $_POST;
            $id_nuevo_grado  = $data['grado'];
            
            unset($data['grado']);
            
            $id_alumno       = $id;
            $ag              = $this->model->get_sibling('alumno_grado');
            $ag->actualizar_grado($id_alumno, $id_nuevo_grado);
            $this->model->change_status($_POST);
            $this->model->save();
            HttpHandler::redirect('/sagradocorazon/alumnos/ver_alumnos?update=ok');
        else:
            echo "llamada realizada fuera de la funcion actualizar";
        endif;
    }
    
    /* Editar informacion de un alumno */

    public function editar_alumno() {
        $id = isset($_GET['id']) ? $_GET['id'] : '0';
        $cache = array();
        if ($this->model->exists($id)) {
            $grado    = $this->model->get_sibling('grado');
            $cache[0] = $this->model->search('id', $id);
            $cache[1] = $grado->get_list();
            $this->view->edicion_alumno($cache);
        } else {
            HttpHandler::redirect('/sagradocorazon/alumnos/ver_alumnos');
        }
    }

    /* Mostrar listado de alumnos */

    public function ver_alumnos() {
        import('scripts.paginacion');

        $user = Session::getUser();
        $cache = array();
        $tipo = "1";
        $strt = "";
        if (isset($_GET['type']) && !empty($_GET['type'])) {

            $strt = $_GET['type'];

            switch ($_GET['type']) {
                case 'activo':
                    $tipo = "1";
                    break;
                case 'inactivo':
                    $tipo = "0";
                    break;
                default:
                    $tipo = "1";
                    break;
            }
        }


        if (isset($_GET['filtro']) && !empty($_GET['filtro'])):
            $filtro = data_model()->sanitizeData($_GET['filtro']);
            $filtroArray = array('primer_nombre' => $filtro, 'segundo_nombre' => $filtro, 'primer_apellido' => $filtro, 'segundo_apellido' => $filtro, "activo" => $tipo);
            $numeroRegistros = $this->model->MultyQuantify($filtroArray);
            $url_filtro = "/sagradocorazon/alumnos/ver_alumnos?filtro=" . $filtro . "&type=$strt&";

            list($paginacion_str, $limitInf, $tamPag) = paginar($numeroRegistros, $url_filtro, 5);

            $cache[0] = $this->model->MultyFilter($filtroArray, $limitInf, $tamPag);
        else:
            $numeroRegistros = $this->model->MultyQuantify(array("activo" => $tipo));
            $url_filtro = "/sagradocorazon/alumnos/ver_alumnos?type=$strt&";

            list($paginacion_str, $limitInf, $tamPag) = paginar($numeroRegistros, $url_filtro, 5);

            $cache[0] = $this->model->MultyFilter(array("activo" => $tipo), $limitInf, $tamPag);
        endif;

        $this->view->mostrar_alumnos($cache, $user, $paginacion_str, $tipo);
    }

    /* Eliminar alumno */

    public function eliminar_alumno() {
        $id = isset($_GET['id']) ? $_GET['id'] : '0';
        $this->model->get($id);
        $this->model->set_attr('activo', '0');
        $this->model->save();
        HttpHandler::redirect("/sagradocorazon/alumnos/ver_alumnos?del=ok");
    }

    /* Activar alumno */

    public function activar_alumno() {
        $id = isset($_GET['id']) ? $_GET['id'] : '0';
        $this->model->get($id);
        $this->model->set_attr('activo', '1');
        $this->model->save();
        HttpHandler::redirect("/sagradocorazon/alumnos/ver_alumnos?act=ok");
    }

}

?>