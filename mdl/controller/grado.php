<?php

import('mdl.model.grado');
import('mdl.view.grado');

class gradoController extends controller {

    public function __construct() {
        if (!Session::singleton()->ValidateSession())
            HttpHandler::redirect('/sagradocorazon/system/login?error=NoAuth');
        if (Session::singleton()->getLevel() != 1)
            HttpHandler::redirect('/sagradocorazon/system/forbiden');
        $this->model = Helper::get_model($this); # load respective module
        $this->view = Helper::get_view($this);  # load respective view
    }

    // funcion que optiene los registros de la BBDD
    public function agregar() {
        $cache = array();       // crea un arreglo llamado cache
        $cache[0] = $this->model->get_list();  // optenemos la lista de registro de la tabla grado 
        $cache[1] = $this->model->get_sibling('docente')->get_list();
        $this->view->agregar_grado($cache);  // se envian los datos a la vista
    }

    // Funcion que guardara los datos ingresado por el usuario
    public function guardar_grado() {
        if (isset($_POST) && !empty($_POST)):       // verificamos que se halla accesido a la url correctamente
            $this->model->get(0);          // crearemos un nuevo objeto
            $this->model->change_status($_POST);      // los elementos pasador por parametros se tranforman en atributos del modelo
            $this->model->save();          // se combierten los atributos y se gurdan en la tabla asociada
            HttpHandler::redirect('/sagradocorazon/grado/agregar');  // se redirecciona a la funcion agregar
        else:
            echo "La funcion no fue llamada desde el formulario";
        endif;
    }

    // Funcion que elimina los registros
    public function eliminar() {
        $idgrado = isset($_GET['id']) ? $_GET['id'] : '0';    // se verifica si el id ingresado existe
        $this->model->delete($idgrado);        // se eliminan un registro de la tabla asociada 
        HttpHandler::redirect('/sagradocorazon/grado/agregar');  // se redirecciona a la funcion agregar
    }

    //Funcion que modificar los registros(solo modificamos la funcion de agregar)
    public function modificar() {
        if (isset($_POST) && !empty($_POST)):
            $id = (isset($_POST['id'])) ? $_POST['id'] : 0;      // se verifica que sea un id 	
            $this->model->get($id);          // agregan los cambios del regitro en
            $this->model->change_status($_POST);      // el mismo id.
            $this->model->save();          //
            HttpHandler::redirect('/sagradocorazon/grado/agregar');  // se redirecciona a la funcion agregar
        else:
            echo "La funcion no fue llamada desde el formulario";
        endif;
    }

    // Funcion para editar los registros.
    public function editar() {
        $id = isset($_GET['id']) ? $_GET['id'] : '0';   // verifica que el id ingresado exista
        $cache = array();         // creamos un arreglo llamado cache
        $cache[0] = $this->model->search('id', $id);  // buscamos el id en la tablas grado y se asigna a cache
        $cache[1] = $this->model->get_sibling('docente')->get_list();
        $this->view->edicion_grado($cache);    // se envian los datos a la vista
    }

    public function generar_reporte() {
        $cache[0] = $this->model->data_reporte();
        $this->view->generar_reporte($cache);
    }

}

?>