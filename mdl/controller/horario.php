<?php

/* Imports necesarios */
import('mdl.model.horario');
import('mdl.view.horario');

/* Definicion de controlador para modulo */

class horarioController extends controller {

    public function __construct() {
        if (!Session::singleton()->ValidateSession()) {
            HttpHandler::redirect('/sagradocorazon/system/login?error=NoAuth');
        }
        $this->model = Helper::get_model($this); # load respective module
        $this->view = Helper::get_view($this); # load respective view
    }

    /* Presentar modulo nuevo horario */

    public function nuevo_horario() {
        $this->view->agregar_horario();
    }

    /* Guardar nuevo horario */

    public function guardar_horario() {
        if (isset($_POST) && !empty($_POST)):
            $this->model->get(0);
            $this->model->change_status($_POST);
            $this->model->save();
            HttpHandler::redirect('/sagradocorazon/horario/nuevo_horario');
        else:
            echo "llamada realizada fuera de la funcion guardar_horario";
        endif;
    }

    /* Mostrar listado de horario */

    public function ver_horario() {
        $cache = array();
        $cache[0] = $this->model->get_list();
        $this->view->mostrar_horario($cache);
    }

    /* Editar informacion de un horario */

    public function editar_horario() {
        $id = isset($_GET['id']) ? $_GET['id'] : '0';
        $cache = array();
        $cache[0] = $this->model->search('id', $id);
        $this->view->edicion_horario($cache);
    }

    /* Eliminar horario */

    public function eliminar_horario() {
        $id = isset($_GET['id']) ? $_GET['id'] : '0';
        $this->model->delete($id);
        HttpHandler::redirect("/sagradocorazon/horario/ver_horario");
    }

    /* Actualizar datos luego de editarlos */

    public function actualizar() {
        if (isset($_POST) && !empty($_POST)):
            $id = isset($_GET['id']) ? $_GET['id'] : 0;
            $this->model->get($id);
            $this->model->change_status($_POST);
            $this->model->save();
            HttpHandler::redirect('/sagradocorazon/horario/ver_horario');
        else:
            echo "llamada realizada fuera de la funcion actualizar";
        endif;
    }

}

?>