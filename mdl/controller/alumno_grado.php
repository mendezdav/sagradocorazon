<?php

import('mdl.view.alumno_grado');
import('mdl.model.alumno_grado');

/**
 * alumno_gradoController
 * clase que servira para manuplar controlador
 * para modulo alumno grado
 */
class alumno_gradoController extends controller {

    public function __construct() {
        if (!Session::singleton()->ValidateSession())
            HttpHandler::redirect('/sagradocorazon/system/login?error=NoAuth');
        $this->model = Helper::get_model($this); # load respective module
        $this->view = Helper::get_view($this); # load respective view
    }

    /**
     * nuevo_alumno_grado genera los modelos necesarios para crear lista de datos de alumno y grado
     * para su posterior visualizacion y creacion de registro
     * @return null
     */
    public function nuevo_alumno_grado() {
        $query = "SELECT alumnos.id,alumnos.primer_nombre,alumnos.segundo_nombre,alumnos.primer_apellido,alumnos.segundo_apellido
			FROM alumno_grado RIGHT JOIN alumnos ON alumnos.id=alumno_grado.id_alumno WHERE alumno_grado.id IS NULL";

        $data_alumno = array();
        $data_grado = array();
        $cache = array();
        $model_grado = $this->model->get_sibling('grado');
        $data_alumno[0] = data_model()->cacheQuery($query);
        $data_grado[0] = $model_grado->get_list();

        data_model()->executeQuery($query); #comprobar que ya no existan registros que agregar
        while ($dat = data_model()->getResult()->fetch_assoc()):
            $cache[] = $dat;
        endwhile;

        if (isset($cache) && !empty($cache)): #si esta vacio
            $this->view->agregar_alumno_grado($data_alumno, $data_grado);
        else:
            HttpHandler::redirect("/sagradocorazon/alumno_grado/ver_alumno_grado?var=no hay alumnos que agregar");
        endif;
    }

    /**
     * guardar_alumno_grado funcion para almacenar un nuevo registro alumno grado
     * @return null
     */
    public function guardar_alumno_grado() {
        $id_grado = $_POST['id_grado']; #Es un nombre no un id
        $query = "SELECT id FROM grado WHERE nombre='$id_grado'";
        $cache = array();
        data_model()->executeQuery($query);
        $cache[] = data_model()->getResult()->fetch_assoc();
        $_POST['id_grado'] = $cache[0]['id']; #aca cambio el nombre por el id que ya encontre

        if (isset($_POST) && !empty($_POST)):
            $this->model->get(0);
            $this->model->change_status($_POST);
            $this->model->save();
            HttpHandler::redirect('/sagradocorazon/alumno_grado/nuevo_alumno_grado');
        else:
            echo "llamada realizada fuera de la funcion guardar_alumno";
        endif;
    }

    /**
     * ver_alumno_grado
     * @return null 
     */
    public function ver_alumno_grado() {
        if (isset($_GET['var'])):
            echo $_GET['var'];
        endif;
        /**
         * $query consulta que selecciona id registro, nombre completo de alumno y el grado
         * @var string
         */
        $query = "SELECT alumno_grado.id,alumnos.primer_nombre,alumnos.segundo_nombre,
			alumnos.primer_apellido,alumnos.segundo_apellido,grado.nombre FROM alumno_grado 
			LEFT JOIN alumnos ON alumno_grado.id_alumno=alumnos.id LEFT JOIN 
			grado ON grado.id=alumno_grado.id_grado";
        $cache = data_model()->cacheQuery($query);
        $this->view->mostrar_alumno_grado($cache);
    }

    /**
     * eliminar_alumno_grado funcion que elimina el registro de la tabla alumno grado
     * @return null
     */
    public function eliminar_alumno_grado() {
        $id = isset($_GET['id']) ? $_GET['id'] : '0';
        $this->model->delete($id);
        HttpHandler::redirect("/sagradocorazon/alumno_grado/ver_alumno_grado");
    }

    /**
     * editar_alumno_grado edita el grado de un alumno especifico
     * @return null
     */
    public function editar_alumno_grado() {
        $model_alumno = $this->model->get_sibling('alumnos');
        $model_grado = $this->model->get_sibling('grado');

        $data_alumno_grado = array();
        $id_alumno_grado = isset($_GET['id_alumno_grado']) ? $_GET['id_alumno_grado'] : '0';
        $query_alumno_grado = "SELECT * FROM alumno_grado WHERE id='$id_alumno_grado'";
        data_model()->executeQuery($query_alumno_grado);
        $datos_alumno_grado = data_model()->getResult()->fetch_assoc();

        $id_alumno = $datos_alumno_grado['id_alumno'];
        $id_grado = $datos_alumno_grado['id_grado'];

        $data_alumno[0] = $model_alumno->search('id', $id_alumno);
        $data_grado[0] = $model_grado->get_list();
        $data_alumno_grado[0] = $this->model->search('id', $id_alumno_grado);

        $this->view->edicion_alumno_grado($data_alumno, $data_grado, $data_alumno_grado);
    }

    /**
     * actualizar guarda los cambios realizados a un registro
     * return null
     */
    public function actualizar() {
        $id_grado = $_POST['id_grado']; #Es un nombre no un id
        $query = "SELECT id FROM grado WHERE nombre='$id_grado'";
        $cache = array();
        data_model()->executeQuery($query);
        $cache[] = data_model()->getResult()->fetch_assoc();
        $_POST['id_grado'] = $cache[0]['id']; #aca cambio el nombre por el id que ya encontre

        if (isset($_POST) && !empty($_POST)):
            $id = isset($_POST['id']) ? $_POST['id'] : 0;
            $this->model->get($id);
            $this->model->change_status($_POST);
            $this->model->save();
            HttpHandler::redirect('/sagradocorazon/alumno_grado/ver_alumno_grado');
        else:
            echo "llamada realizada fuera de la funcion actualizar";
        endif;
    }
    
    public function obtener_grado(){
        if(isset($_POST) && !empty($_POST)){
            $id_grado  = $this->model->obtener_grado($_POST['id_alumno']);
            $grado     = $this->model->get_sibling('grado');
            $grado->get($id_grado);
            $response = array();
            $response['nombre'] = $grado->get_attr('nombre');
            $response['id']     = $id_grado;
            echo json_encode($response);
        }
    }

}

?>