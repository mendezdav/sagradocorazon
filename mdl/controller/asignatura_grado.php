<?php

import('mdl.model.asignatura_grado');
import('mdl.view.asignatura_grado');

/**
 * asignatura_gradoController clase para manipular controlador de modulo asignatura_grado
 */
class asignatura_gradoController extends controller {

    public function __construct() {
        if (!Session::singleton()->ValidateSession())
            HttpHandler::redirect('/sagradocorazon/system/login?error=NoAuth');
        if (Session::singleton()->getLevel() != 1)
            HttpHandler::redirect('/sagradocorazon/system/forbiden');
        $this->model = Helper::get_model($this); # load respective module
        $this->view = Helper::get_view($this);  # load respective view
    }

    /**
     * nueva_asignatura_grado genera los id de los elementos a mostrar para generar un registro
     * @return null 
     */
    public function nueva_asignatura_grado() {
        /**
         * $query mostrar datos de docente que no tienen registro en la tabla asignatura_grado
         * @var string
         */
        $elementos = 0;
        $id_grado = $_GET['id_grado'];
        $query = "select * from asignatura where not exists(select 1 from asignatura_grado where id_asignatura= asignatura.id AND 
								  id_grado='$id_grado')";

        data_model()->executeQuery($query);
        $data = data_model()->getResult()->fetch_assoc();
        if (data_model()->getNumRows() == 0):
            HttpHandler::redirect('/sagradocorazon/asignatura_grado/ver_asignatura_grado?var=No existen mas asignaturas en el grado');
        else:

            $docente_model = $this->model->get_sibling('docente');
            $data_docente[0] = $docente_model->get_list();
            $data_asignatura[0] = data_model()->cacheQuery($query);
            $oG = $this->model->get_sibling('grado');
            $oG->get($id_grado);
            $nombre_grado = $oG->get_attr('nombre');
            $this->view->agregar_asignatura_grado($data_docente, $data_asignatura, $id_grado, $nombre_grado);
        endif;
    }

    /**
     * guardar_asignatura_grado guarda un nuevo registro a la tabla
     * @return null
     */
    public function guardar_asignatura_grado() {
        if (isset($_POST) && !empty($_POST)):
            $_POST['id_grado'] = $_GET['id_grado'];
            $_POST['id_asignatura'] = $_GET['id_asignatura'];
            $id_grado = $_POST['id_grado'];
            $this->model->get(0);
            $this->model->change_status($_POST);
            $this->model->save();
            HttpHandler::redirect('/sagradocorazon/asignatura_grado/nueva_asignatura_grado?id_grado=' . $id_grado);
        else:
            echo "llamada realizada fuera de la funcion";
        endif;
    }

    /**
     * ver_asignatura_grado genera lo necesario para mostrar registros de la tabla
     * @return [type] [description]
     */
    public function ver_asignatura_grado() {
        if (isset($_GET['var'])):
            echo $_GET['var']; #presenta un mensaje si se direcciono
        endif;

        /**
         * $query selecciona datos a presentar en tabla, estos son aquellos que tengan registro de la tabla
         * asignatura_grado
         * @var string
         */
        $query = "SELECT asignatura_grado.id,docente.primer_nombre,docente.segundo_nombre,docente.primer_apellido,
			docente.segundo_apellido, grado.nombre AS nombre_grado,asignatura.nombre AS nombre_asignatura FROM asignatura_grado LEFT JOIN docente
			ON docente.id=asignatura_grado.id_docente LEFT JOIN grado ON grado.id=asignatura_grado.id_grado
			LEFT JOIN asignatura ON asignatura.id=asignatura_grado.id_asignatura";

        $cache[0] = data_model()->cacheQuery($query);
        $this->view->mostrar_alumno_grado($cache);
    }

    /**
     * edicion_alumno_grado modifica informacion de un registro en especifico
     * @return null
     */
    public function editar_asignatura_grado() {
        $id = isset($_GET['id_asignatura_grado']) ? $_GET['id_asignatura_grado'] : '0';
        $model_asignatura = $this->model->get_sibling('asignatura');
        $model_grado = $this->model->get_sibling('grado');
        $model_docente = $this->model->get_sibling('docente');

        $query = "SELECT * FROM asignatura_grado WHERE id='$id'";
        data_model()->executeQuery($query);
        $data_asignatura_grado = data_model()->getResult()->fetch_assoc();
        $id_asignatura = $data_asignatura_grado['id_asignatura'];
        $id_grado = $data_asignatura_grado['id_grado'];
        $id_docente = $data_asignatura_grado['id_docente'];

        $query = "select nombre from asignatura where not exists(select 1 from asignatura_grado where id_asignatura= asignatura.id AND 
								  id_grado='$id_grado')";


        #$data_asignatura[0]=$model_asignatura->get_list();
        $data_asignatura[0] = data_model()->cacheQuery($query);
        $data_grado[0] = $model_grado->search('id', $id_grado);
        $data_docente[0] = $model_docente->search('id', $id_docente);

        /*         * ** */
        $query = "SELECT asignatura_grado.id, asignatura_grado.id_docente FROM asignatura_grado RIGHT JOIN docente ON docente.id=asignatura_grado.id_docente
			WHERE asignatura_grado.id='$id'";
        $data_asignatura_grado[0] = data_model()->cacheQuery($query);

        $this->view->edicion_asignatura_grado($data_asignatura, $data_grado, $data_docente, $data_asignatura_grado);
    }

    /**
     * eliminar_alumno_grado tomando el ID del registro se procede a eliminar
     * @return null
     */
    public function eliminar_asignatura_grado() {
        $id = isset($_GET['id_asignatura_grado']) ? $_GET['id_asignatura_grado'] : '0';
        $this->model->delete($id);
        HttpHandler::redirect("/sagradocorazon/asignatura_grado/ver_asignatura_grado");
    }

    /**
     * actualizar realiza cambios despues de haberlo editado
     * @return null
     */
    public function actualizar() {
        if (isset($_POST) && !empty($_POST)):

            $id_grado = $_POST['id_grado']; #Es un nombre no un id
            $query = "SELECT id FROM grado WHERE nombre='$id_grado'";
            $cache = array();
            data_model()->executeQuery($query);
            $cache[] = data_model()->getResult()->fetch_assoc();
            $_POST['id_grado'] = $cache[0]['id']; #aca cambio el nombre por el id que ya encontre
            $id_asignatura = $_POST['id_asignatura']; #Es un nombre no un id
            $query = "SELECT id FROM asignatura WHERE nombre='$id_asignatura'";
            data_model()->executeQuery($query);
            $cache[] = data_model()->getResult()->fetch_assoc();
            $_POST['id_asignatura'] = $cache[1]['id']; #aca cambio el nombre por el id que ya encontre
            var_dump($_POST);

            $id = isset($_POST['id']) ? $_POST['id'] : 0;
            $this->model->get($id);
            $this->model->change_status($_POST);
            $this->model->save();
            HttpHandler::redirect('/sagradocorazon/asignatura_grado/ver_asignatura_grado');
        else:
            echo "llamada realizada fuera de la funcion actualizar";
        endif;
    }

}

?>