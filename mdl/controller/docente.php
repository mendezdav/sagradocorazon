<?php

import('mdl.view.docente');
import('mdl.model.docente');

/**
 * 
 */
class docenteController extends controller {

    public function __construct() {
        if (!Session::singleton()->ValidateSession())
            HttpHandler::redirect('/sagradocorazon/system/login?error=NoAuth');
        if (Session::singleton()->getLevel() != 1)
            HttpHandler::redirect('/sagradocorazon/system/forbiden');
        $this->model = Helper::get_model($this); # load respective module
        $this->view = Helper::get_view($this);  # load respective view
    }

    public function agregar() {
        $this->view->agregar();
    }

    public function listado() {
        $user = Session::getUser();
        import('scripts.paginacion');
        $cache = array();
        if (isset($_GET['filtro']) && !empty($_GET['filtro'])):
            $filtro = data_model()->sanitizeData($_GET['filtro']);
            $filtroArray = array('primer_nombre' => $filtro, 'segundo_nombre' => $filtro, 'primer_apellido' => $filtro, 'segundo_apellido' => $filtro);
            $numeroRegistros = $this->model->MultyQuantify($filtroArray);
            $url_filtro = "/sagradocorazon/docente/listado?filtro=" . $filtro . "&";
            list($paginacion_str, $limitInf, $tamPag) = paginar($numeroRegistros, $url_filtro, 5);
            $cache[0] = $this->model->MultyFilter($filtroArray, $limitInf, $tamPag);
        else:
            $numeroRegistros = $this->model->quantify();
            $url_filtro = "/sagradocorazon/docente/listado?";
            list($paginacion_str, $limitInf, $tamPag) = paginar($numeroRegistros, $url_filtro, 5);
            $cache[0] = $this->model->get_list($limitInf, $tamPag);
        endif;
        $this->view->listado($cache, $user, $paginacion_str);
    }

    public function guardar() {
        if (isset($_POST) && !empty($_POST)):
            $id = (isset($_POST['id']) && !empty($_POST['id'])) ? $_POST['id'] : 0;
            $this->model->get($id);

            $data = $_POST;

            if ($id == 0)
                $data['usuario'] = $this->model->createUsername($id, $data['primer_nombre']);
            else
                $data['usuario'] = $this->model->get_attr('usuario');
            if ($id == 0)
                $data['clave'] = md5($data['usuario']);
            else
                $data['clave'] = $this->model->get_attr('clave');

            $data['adminrights'] = $data['permiso'];

            unset($data['permiso']);

            $this->model->change_status($data);

            if (intval($data['adminrights']) == 0)
                $this->model->set_attr('adminrights', '0');

            $this->model->save();

            if ($id == 0)
                HttpHandler::redirect('/sagradocorazon/docente/agregar?save=ok');
            else
                HttpHandler::redirect('/sagradocorazon/docente/agregar?update=ok');
        else:
            echo "La funcion no fue llamada desde el formulario";
        endif;
    }

    public function eliminar() {
        if (isset($_GET['id']) && !empty($_GET['id'])) {

            $id = $_GET['id'];

            if ($this->model->exists($id)) {

                $isDroppable = $this->model->is_droppable($id);

                if ($isDroppable) {

                    $this->model->delete($id);
                    HttpHandler::redirect('/sagradocorazon/docente/listado?success=deleted');
                } else {

                    HttpHandler::redirect('/sagradocorazon/docente/listado?err=dependencies');
                }
            } else {

                HttpHandler::redirect('/sagradocorazon/docente/listado');
            }
        } else {

            HttpHandler::redirect('/sagradocorazon/docente/listado');
        }
    }

    public function get_info() {

        $idOrientador = $_POST['idOrientador'];

        $this->model->get($idOrientador);

        $response = array();

        $response['primer_nombre'] = $this->model->primer_nombre;
        $response['segundo_nombre'] = $this->model->segundo_nombre;
        $response['primer_apellido'] = $this->model->primer_apellido;
        $response['segundo_apellido'] = $this->model->segundo_apellido;
        $response['sexo'] = $this->model->sexo;

        echo json_encode($response);
    }

    public function get_full_info() {
        $id = $_POST['id_docente'];
        $response = array();
        $existe = $this->model->exists($id);
        $response['found'] = $existe;
        $response['data'] = null;

        if ($existe) {
            $this->model->get($id);

            $fields = $this->model->get_fields();
            $response['data'] = array();

            foreach ($fields as $field) {

                $response['data'][$field] = $this->model->get_attr($field);
            }
        }

        echo json_encode($response);
    }

    public function auth() {

        $response = array();
        $response['authStatus'] = false;
        $oldpass = $_POST['oldpass'];

        $id = $this->model->getIdByUserName(Session::getUser());

        $this->model->get($id);

        $currentpass = $this->model->get_attr('clave');

        if ($currentpass == md5($oldpass)) {

            $response['authStatus'] = true;
        }

        echo json_encode($response);
    }

    public function actualizar_clave() {

        $clave = $_POST['newpassword'];
        $id = $this->model->getIdByUserName(Session::getUser());

        $this->model->get($id);
        $this->model->set_attr('clave', md5($clave));

        $res = $this->model->save();

        if ($res)
            HttpHandler::redirect('/sagradocorazon/system/account?update=ok');
        else
            HttpHandler::redirect('/sagradocorazon/system/account?update=err');
    }

}

?>