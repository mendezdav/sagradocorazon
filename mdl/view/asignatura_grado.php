<?php

/**
 * Clase que manejara vista del modulo asignatura_grado
 */
class asignatura_gradoView {

    /**
     * agregar_asignatura_grado
     * @param  id $data_docente          nombre completo del docente
     * @param  id $data_asignatura       todos los nombres de asignaturas 
     * @param  id $id_grado              id grado
     * @return null                
     */
    public function agregar_asignatura_grado($data_docente, $data_asignatura, $id_grado, $nombre_grado) {
        template()->buildFromTemplates('template.html');
        template()->addTemplateBit('contenido','nueva_asignatura_grado.html');
        
        
        page()->addEstigma('id_grado', $id_grado);
        page()->addEstigma('nombre_grado', $nombre_grado);
        page()->addEstigma('lista_docente', array('SQL', $data_docente[0]));
        page()->addEstigma('lista_asignatura', array('SQL', $data_asignatura[0]));
        
        isAdmin();
        isSecretary();
        
        template()->parseOutput();
        template()->parseExtras();
        print page()->getContent();
    }

    /**
     * mostrar_alumno_grado
     * @param  id $cache todos los registros de la tabla
     * @return null
     */
    public function mostrar_alumno_grado($cache) {
        template()->buildFromTemplates('template.html');
        template()->addTemplateBit('contenido','ver_asignatura_grado.html');
        
        page()->setTitle('Gestión de cursos');
        
        page()->addEstigma('activeModule', str_replace('View', '', get_class($this)));
        page()->addEstigma('username', Session::getUser());
        
        page()->addEstigma('asignatura_grado', array('SQL', $cache[0]));
        template()->parseOutput();
        template()->parseExtras();
        print page()->getContent();
    }
    
    /**
     * edicion_alumno_grado 
     * @param  id $data_asignatura       todas las asignaturas
     * @param  id $data_grado            todos los grados
     * @param  id $data_docente          nombre de docente especifico
     * @param  id $data_asignatura_grado id de registro y de docenete
     * @return null                        
     */
    public function edicion_asignatura_grado($data_asignatura, $data_grado, $data_docente, $data_asignatura_grado) {
        template()->buildFromTemplates('editar_asignatura_grado.html');
        page()->addEstigma('lista_asignatura', array('SQL', $data_asignatura[0]));
        page()->addEstigma('lista_grado', array('SQL', $data_grado[0]));
        page()->addEstigma('lista_docente', array('SQL', $data_docente[0]));
        page()->addEstigma('lista_asignatura_grado', array('SQL', $data_asignatura_grado[0]));
        template()->parseOutput();
        print page()->getContent();
    }

}

?>
