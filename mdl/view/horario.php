<?php

/* Definicion de vista para modulo de horario */

class horarioView {
    /* Presentar formulario para agregar nuevo horario */

    public function agregar_horario() {
        template()->buildFromTemplates('nuevo_horario.html');
        print page()->getContent();
    }

    /* Presentar tabla con listado del horario */

    public function mostrar_horario($cache) {
        template()->buildFromTemplates('ver_horario.html');
        page()->addEstigma('lista_horario', array('SQL', $cache[0]));
        template()->parseOutput();
        print page()->getContent();
    }

    /* Presentacion de datos del horario que se modificara */

    public function edicion_horario($cache) {
        template()->buildFromTemplates('editar_horario.html');
        page()->addEstigma('datos_horario', array('SQL', $cache[0]));
        template()->parseOutput();
        print page()->getContent();
    }

}

?>