<?php

class asignaturaView {

    //funcion que resiva los datos para mostarlos
    public function agregar_asignatura($cache) {
        template()->buildFromTemplates('template.html');
        template()->addTemplateBit('contenido', 'agregar_asignatura.html'); // carga el archivo html ha mostrar

        page()->setTitle('Asignatura');

        page()->addEstigma('activeModule', str_replace('View', '', get_class($this)));
        page()->addEstigma('username', Session::getUser());
        page()->addEstigma('lista_asignatura', array('SQL', $cache[0]));

        isAdmin();
        isSecretary();

        template()->parseExtras();
        template()->parseOutput(); // se remplazan las etiquetas

        print page()->getContent(); // carga el contenido de la pagina y lo muestra en pantalla 
    }

    public function editar_asignatura($cache) {
        template()->buildFromTemplates('template.html');
        template()->addTemplateBit('contenido', 'editar_asignatura.html');

        page()->setTitle('Asignatura - Editar');

        page()->addEstigma('activeModule', str_replace('View', '', get_class($this)));
        page()->addEstigma('username', Session::getUser());
        page()->addEstigma('dato_asignatura', array('SQL', $cache[0]));

        isAdmin();
        isSecretary();

        template()->parseExtras();
        template()->parseOutput();

        print page()->getContent();
    }

}

?>