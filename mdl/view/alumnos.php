<?php

/* Definicion de vista para modulo de alumnos */

class alumnosView {
    /* Presentar formulario para agregar nuevo alumno */

    public function agregar_alumno($cache) {
        template()->buildFromTemplates('template.html');
        template()->addTemplateBit('contenido', 'nuevo_alumno.html');

        page()->setTitle('Alumnos');

        page()->addEstigma('activeModule', str_replace('View', '', get_class($this)));
        page()->addEstigma('username', Session::getUser());
        page()->addEstigma('grados', array('SQL', $cache[0]));

        isAdmin();
        isSecretary();

        @template()->parseOutput();
        template()->parseExtras();

        print page()->getContent();
    }

    /* Presentar tabla con listado de alumnos */

    public function mostrar_alumnos($cache, $user, $paginacion_str, $tipo) {
        template()->buildFromTemplates('template.html');
        template()->addTemplateBit('contenido', 'ver_alumnos.html');

        page()->setTitle('Alumnos - Listado');

        page()->addEstigma('lista_alumnos', array('SQL', $cache[0]));
        page()->addEstigma('activeModule', str_replace('View', '', get_class($this)));
        page()->addEstigma('username', $user);
        page()->addEstigma('tsr', $tipo);
        page()->addEstigma('pag', $paginacion_str);

        isAdmin();
        isSecretary();

        template()->parseOutput();
        template()->parseExtras();

        print page()->getContent();
    }

    /* Presentacion de datos de alumno que se modificara */

    public function edicion_alumno($cache) {
        template()->buildFromTemplates('template.html');
        template()->addTemplateBit('contenido', 'editar_alumno.html');

        page()->setTitle('Alumnos - Editar');

        page()->addEstigma('activeModule', str_replace('View', '', get_class($this)));
        page()->addEstigma('username', Session::getUser());
        page()->addEstigma('grados', array('SQL', $cache[1]));
        page()->addEstigma('datos_alumno', array('SQL', $cache[0]));

        isAdmin();
        isSecretary();

        template()->parseOutput();
        template()->parseExtras();

        print page()->getContent();
    }

}

?>