<?php

/* definicion de la vista */

class systemView {

    public function index($weakProtection, $cursos, $asignaturas) {
        if (!Session::singleton()->ValidateSession())
            HttpHandler::redirect('/sagradocorazon/system/login?error=NoAuth');

        template()->buildFromTemplates('template.html');
        template()->addTemplateBit('contenido', 'index.html');
        
        page()->addEstigma('activeModule', str_replace('View', '', get_class($this)));
        
        isAdmin();
        isSecretary();

        /* aviso para cambio de clave */
        if ($weakProtection)
            page()->addEstigma('weakProtection', true);
        else
            page()->addEstigma('weakProtection', false);

        if($cursos['vacio']){
            page()->addEstigma('cursos_docente', 'No tiene cursos asignados');
        }else{
            template()->addTemplateBit('cursos_docente', 'listado_cursos.html');
            page()->addEstigma('lista_cursos', array('SQL', $cursos['data']));
        }
        
        if($asignaturas['vacio']){
            page()->addEstigma('asignaturas_docente', 'No tiene asignaturas asignadas');
        }else{
            template()->addTemplateBit('asignaturas_docente', 'lista_asignaturas.html');
            page()->addEstigma('lista_asignaturas', array('SQL', $asignaturas['data']));
        }
        
        page()->addEstigma('username', Session::getUser());
        page()->setTitle('Inicio - sistema');
        
        template()->parseExtras();
        @template()->parseOutput();
        
        print page()->getContent();
    }

    public function account() {
        if (!Session::singleton()->ValidateSession())
            HttpHandler::redirect('/sagradocorazon/system/login?error=NoAuth');
        template()->buildFromTemplates('template.html');
        page()->addEstigma('activeModule', str_replace('View', '', get_class($this)));
        isAdmin();
        isSecretary();
        page()->addEstigma('username', Session::getUser());
        template()->addTemplateBit('contenido', 'cuenta.html');
        page()->setTitle('Cuenta - administracion');
        template()->parseExtras();
        template()->parseOutput();
        print page()->getContent();
    }
    
    public function evaluaciones($nombre_asignatura, $nombre_grado, $dataArray, $cache, $id_asignatura, $id_grado){
        if (!Session::singleton()->ValidateSession())
            HttpHandler::redirect('/sagradocorazon/system/login?error=NoAuth');
        
        template()->buildFromTemplates('template.html');
        
        page()->setTitle('Evaluaciones');
        
        template()->addTemplateBit('contenido', 'evaluaciones.html');
        page()->addEstigma('activeModule', str_replace('View', '', get_class($this)));
        page()->addEstigma('username', Session::getUser());
        page()->addEstigma('asignatura', $nombre_asignatura);
        page()->addEstigma('grado', $nombre_grado);
        page()->addEstigma('id_grado', $id_grado);
        page()->addEstigma('id_asignatura', $id_asignatura);
        page()->addEstigma('data', array('SQL', $cache[0]));     // informacion del reporte
        
        for($i = 0; $i < count($dataArray); $i++){
            page()->addEstigma("{$dataArray[$i]['numrow']}", $i+1);
        }
        
        isAdmin();
        isSecretary();
        
        template()->parseExtras();
        @template()->parseOutput();
        
        print page()->getContent();
    }

    public function login($cache = null) {
        if (Session::singleton()->ValidateSession())
            HttpHandler::redirect('/sagradocorazon/system/index');
        template()->buildFromTemplates('template_log.html');
        template()->addTemplateBit('contenido', 'login.html');
        page()->setTitle('Login - sistema');
        template()->parseExtras();
        @template()->parseOutput();
        print page()->getContent();
    }

    public function forbiden($cache = null) {
        template()->buildFromTemplates('forbiden.html');
        page()->setTitle('Error 403');
        template()->parseExtras();
        print page()->getContent();
    }
    
    public function imprimir_asistencia($cache, $nombre_grado, $dataArray){
        import('common.plugins.sigma.demos.export_php.html2pdf.html2pdf');

        template()->buildFromTemplates('templateReport.html');
        template()->addTemplateBit('report_content', 'asistencia.html');
        
        page()->addEstigma('grado', $nombre_grado);
        page()->addEstigma('APP_PATH', APP_PATH);               // localizacion de recursos estaticos
        page()->addEstigma('username', Session::getUser());     // usuario que genera el reporte
        page()->addEstigma('time', date("y/m/d h:m:s"));        // hora a la que se genera el reporte
        page()->addEstigma('data', array('SQL', $cache[0]));     // informacion del reporte
        page()->addEstigma('fecha', date("y/m/d"));
        
        for($i = 0; $i < count($dataArray); $i++){
            page()->addEstigma("{$dataArray[$i]['numrow']}", $i+1);
        }
        
        template()->parseOutput();                              // parsear salida

        $html2pdf = new HTML2PDF('P', 'letter', 'es');
        $html2pdf->WriteHTML(page()->getContent());             // redireccionar salida hacia pdf
        $html2pdf->Output('asistencia.pdf');                        // generar pdf
    }
    
    public function inscritos($cache, $nombre_grado, $dataArray, $id_grado){

        template()->buildFromTemplates('template.html');
        template()->addTemplateBit('contenido', 'inscritos.html');
        
        isAdmin();
        isSecretary();
        
        page()->addEstigma('grado', $nombre_grado);
        page()->addEstigma('id_grado', $id_grado);
        page()->addEstigma('APP_PATH', APP_PATH);               // localizacion de recursos estaticos
        page()->addEstigma('username', Session::getUser());     // usuario que genera el reporte
        page()->addEstigma('time', date("y/m/d h:m:s"));        // hora a la que se genera el reporte
        page()->addEstigma('data', array('SQL', $cache[0]));     // informacion del reporte
        
        for($i = 0; $i < count($dataArray); $i++){
            page()->addEstigma("{$dataArray[$i]['numrow']}", $i+1);
        }
        
        template()->parseExtras();
        template()->parseOutput();                              // parsear salida

        print page()->getContent();
    }
    
    public function ficha($cache, $nombre_alumno, $nombre_grado, $nombre_docente, $id_alumno,$id_grado){
        template()->buildFromTemplates('template.html');
        template()->addTemplateBit('contenido', 'ficha.html');
    
        isAdmin();
        isSecretary();
        page()->addEstigma('notas', array('SQL', $cache[0]));
        page()->addEstigma('username', Session::getUser());
        page()->addEstigma('nombre_alumno', $nombre_alumno);
        page()->addEstigma('grado', $nombre_grado);
        page()->addEstigma('id_alumno', $id_alumno);
        page()->addEstigma('id_grado', $id_grado);
        page()->addEstigma('orientador', $nombre_docente);
        page()->addEstigma('anio', date("Y"));
        
        template()->parseExtras();
        template()->parseOutput();                              // parsear salida

        print page()->getContent();
    }
    
    public function calificar($cache, $id_ficha_d, $nombre_alumno, $nombre_grado){
        template()->buildFromTemplates('template.html');
        template()->addTemplateBit('contenido', 'calificar.html');
        isAdmin();
        isSecretary();
        page()->addEstigma('notas', array('SQL', $cache[0]));
        page()->addEstigma('username', Session::getUser());
        page()->addEstigma('id_ficha_alumno_d', $id_ficha_d);
        page()->addEstigma('nombre_alumno', $nombre_alumno);
        page()->addEstigma('nombre_grado', $nombre_grado);
        
        template()->parseExtras();
        template()->parseOutput();                              // parsear salida

        print page()->getContent();
    }
    
    public function informe($cache, $nombre_alumno, $nombre_grado, $nombre_docente, $id_alumno){
        import('common.plugins.sigma.demos.export_php.html2pdf.html2pdf');
        template()->buildFromTemplates('templateReport.html');
        template()->addTemplateBit('report_content', 'informe.html');
    
        page()->addEstigma('APP_PATH', APP_PATH);               // localizacion de recursos estaticos
        page()->addEstigma('time', date("y/m/d h:m:s"));        // hora a la que se genera el reporte
        page()->addEstigma('notas', array('SQL', $cache[0]));
        page()->addEstigma('username', Session::getUser());
        page()->addEstigma('nombre_alumno', $nombre_alumno);
        page()->addEstigma('grado', $nombre_grado);
        page()->addEstigma('id_alumno', $id_alumno);
        page()->addEstigma('orientador', $nombre_docente);
        page()->addEstigma('anio', date("Y"));
        
        template()->parseOutput();                              // parsear salida

        $html2pdf = new HTML2PDF('P', 'letter', 'es');
        $html2pdf->WriteHTML(page()->getContent());             // redireccionar salida hacia pdf
        $html2pdf->Output('informe.pdf');                        // generar pdfprint page()->getContent();
    }

}

?>