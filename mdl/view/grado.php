<?php

class gradoView {

    //funciom que resiva los datos para mostarlos
    public function agregar_grado($cache) {
        template()->buildFromTemplates('template.html');
        template()->addTemplateBit('contenido', 'agregar_grado.html'); // carga el archivo html ha mostrar
        page()->addEstigma('activeModule', str_replace('View', '', get_class($this)));
        page()->addEstigma('username', Session::getUser());
        page()->setTitle('Grados');
        isAdmin();
        isSecretary();
        page()->addEstigma('lista_grado', array('SQL', $cache[0]));  // metodo que manejara la pagina
        page()->addEstigma('docentes', array('SQL', $cache[1]));
        template()->parseOutput();          // se remplazan las etiquetas
        template()->parseExtras();
        print page()->getContent();          // carga el contenido de la pagina y lo muestra en pantalla 
    }

    // funcion que muestra el registro a modificar
    public function edicion_grado($cache) {
        template()->buildFromTemplates('template.html');
        template()->addTemplateBit('contenido', 'editar_grado.html');   // carga el archivo html ha mostrar
        isAdmin();
        isSecretary();
        page()->setTitle('Grados - Editar');
        page()->addEstigma('docentes', array('SQL', $cache[1]));
        page()->addEstigma('dato_grado', array('SQL', $cache[0]));  // metodo que maneja la pagina
        page()->addEstigma('activeModule', str_replace('View', '', get_class($this)));
        page()->addEstigma('username', Session::getUser());
        template()->parseOutput();          //	remplaza las etiquetas
        template()->parseExtras();
        print page()->getContent();          // carga el contenido de la pagina y lo muestra en pantalla
    }

    public function generar_reporte($cache) {
        import('common.plugins.sigma.demos.export_php.html2pdf.html2pdf');

        template()->buildFromTemplates('templateReport.html');
        template()->addTemplateBit('report_content', 'reporteGrado.html');
        
        page()->addEstigma('APP_PATH', APP_PATH);               // localizacion de recursos estaticos
        page()->addEstigma('username', Session::getUser());     // usuario que genera el reporte
        page()->addEstigma('time', date("y/m/d h:m:s"));        // hora a la que se genera el reporte
        page()->addEstigma('data', array('SQL', $cache[0]));     // informacion del reporte

        template()->parseOutput();                              // parsear salida

        $html2pdf = new HTML2PDF('P', 'letter', 'es');
        $html2pdf->WriteHTML(page()->getContent());             // redireccionar salida hacia pdf
        $html2pdf->Output('grados.pdf');                        // generar pdf
    }

}

?>