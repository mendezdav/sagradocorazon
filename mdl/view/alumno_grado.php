<?php

/**
 * clase que manejara la vista 
 * para modulo alumno grado
 * 
 */
class alumno_gradoView {

    /**
     * agregar_alumno_grado 
     * @param  id $data_alumno elementos de alumno utilizados para poder ingresar un registro
     * @param  id $data_grado  todos los nombres de grados existentes
     * @return null
     */
    public function agregar_alumno_grado($data_alumno, $data_grado) {

        template()->buildFromTemplates('nuevo_alumno_grado.html');

        page()->addEstigma('lista_alumno', array('SQL', $data_alumno[0]));
        page()->addEstigma('lista_grado', array('SQL', $data_grado[0]));

        template()->parseOutput();

        print page()->getContent();
    }

    /**
     * mostrar_alumno_grado 
     * @param  id $cache datos de alumno y grado de registros de la tabla
     * @return null
     */
    public function mostrar_alumno_grado($cache) {
        template()->buildFromTemplates('ver_alumno_grado.html');

        page()->addEstigma('alumno_grado', array('SQL', $cache));

        template()->parseOutput();

        print page()->getContent();
    }

    /**
     * edicion_alumno_grado 
     * @param  id $data_alumno      datos de un alumno 
     * @param  id $data_grado       datos de un grado
     * @param  id $data_alumno_grado datos del registro especifico al que se modificara
     * @return                     
     */
    public function edicion_alumno_grado($data_alumno, $data_grado, $data_alumno_grado) {
        template()->buildFromTemplates('editar_alumno_grado.html');

        page()->addEstigma('lista_alumno', array('SQL', $data_alumno[0]));
        page()->addEstigma('lista_alumno_grado', array('SQL', $data_alumno_grado[0]));
        page()->addEstigma('lista_grados', array('SQL', $data_grado[0]));

        template()->parseOutput();

        print page()->getContent();
    }

}

?>