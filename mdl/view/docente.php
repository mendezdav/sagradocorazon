<?php

/*
 *
 */

class docenteView {

    public function agregar() {
        template()->buildFromTemplates('template.html');
        template()->addTemplateBit('contenido', 'agregar_docente.html'); // carga el archivo html ha mostrar
        page()->setTitle('Docentes');
        page()->addEstigma('activeModule', str_replace('View', '', get_class($this)));
        page()->addEstigma('username', Session::getUser());
        isAdmin();
        isSecretary();
        template()->parseOutput();          // se remplazan las etiquetas
        template()->parseExtras();
        print page()->getContent();
    }

    public function listado($cache, $usuario, $paginacion_str) {
        template()->buildFromTemplates('template.html');
        template()->addTemplateBit('contenido', 'listado_docentes.html'); // carga el archivo html ha mostrar
        page()->setTitle('Docentes - Listado');
        page()->addEstigma('activeModule', str_replace('View', '', get_class($this)));
        page()->addEstigma('username', $usuario);
        page()->addEstigma('pag', $paginacion_str);
        page()->addEstigma('docentes', array('SQL', $cache[0]));
        isAdmin();
        isSecretary();
        template()->parseOutput();          // se remplazan las etiquetas
        template()->parseExtras();
        print page()->getContent();
    }

}

?>